﻿using System;
using ConVoy.Resources.Localization;
using Newtonsoft.Json;

namespace ConVoy
{
	public class ConvoyerModel
	{
		public string id { get; set; }
		public UserModel User { get; set; }
		//public ConvoyModel convoy { get; set; }
		public string convoy { get; set; }
		public double latitude { get; set; }
		public double longitude { get; set; }
		public string device_token { get; set; }
		public int status { get; set; }

		[JsonIgnore]
		public string FirstNameConvoyer
		{
			get
			{
				if (User != null)
					return User.first_name;
				return "";
			}
		}

		[JsonIgnore]
		public string LastNameConvoyer
		{
			get
			{
				if (User != null)
					return User.last_name;
				return "";
			}
		}

		[JsonIgnore]
		public string UsernameConvoyer
		{
			get
			{
				if (User != null)
					return User.username;
				return "";
			}
		}

		[JsonIgnore]
		public string StatusConvoyer
		{
			get
			{
				if (status == 1)
					return LocalizationRes.StatusConvoyer;
				return "";
			}
		}
	}
}
