﻿using System;
namespace ConVoy
{
	public class UserSearchModel
	{
		public string id { get; set; }
		public string username { get; set; }
		public string first_name { get; set; }
		public string last_name { get; set; }
		public string email { get; set; }
	}
}
