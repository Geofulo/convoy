﻿using System;
namespace ConVoy
{
	public class ConvoyModel
	{
		public string id { get; set; }
		public string user { get; set; }
		public string name { get; set; }
		public double latitude_destination { get; set; }
		public double longitude_destination { get; set; }
		public int status { get; set; }
	}
}
