﻿using System;
using ConVoy.Resources.Localization;
using Newtonsoft.Json;

namespace ConVoy
{
	public class FriendModel
	{
		public string id { get; set; }
		public UserModel user1 { get; set; }
		public UserModel user2 { get; set; }
		public int status { get; set; }

		[JsonIgnore]
		public string FirstNameFriend
		{
			get
			{
				if (user1.id == App.CurrentUser.id)
					return user2.first_name;
				else
					return user1.first_name;
			}
		}

		[JsonIgnore]
		public string LastNameFriend
		{
			get
			{
				if (user1.id == App.CurrentUser.id)
					return user2.last_name;
				else
					return user1.last_name;
			}
		}

		[JsonIgnore]
		public string UsernameFriend
		{
			get
			{
				if (user1.id == App.CurrentUser.id)
					return user2.username;
				else
					return user1.username;
			}
		}

		[JsonIgnore]
		public string StatusFriend
		{
			get
			{
				if (status == 1)
				{
					if (user2.id == App.CurrentUser.id)
						return LocalizationRes.StatusFriend_1;
					else 
						return LocalizationRes.StatusFriend_2;
				}
				else
					return "";
			}
		}

		[JsonIgnore]
		public string DeleteItemText
		{
			get
			{
				if (status == 1)
				{
					if (user2.id == App.CurrentUser.id)
						return LocalizationRes.DeleteItemText_1;
					else
						return LocalizationRes.DeleteItemText_2;
				}
				else
					return LocalizationRes.DeleteItemText_2;
			}
		}
	}
}
