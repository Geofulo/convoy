﻿using System;
namespace ConVoy
{
	public class FriendPostModel
	{
		public string id { get; set; }
		public string user1 { get; set; }
		public string user2 { get; set; }
		public int status { get; set; }
	}
}
