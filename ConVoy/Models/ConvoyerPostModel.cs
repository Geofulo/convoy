﻿using System;
namespace ConVoy
{
	public class ConvoyerPostModel
	{
		public string id { get; set; }
		public string user { get; set; }
		public string convoy { get; set; }
		public double latitude { get; set; }
		public double longitude { get; set; }
		public int status { get; set; }
		public string device_token { get; set; }
	}
}
