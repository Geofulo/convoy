﻿using System;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class SearchUsersTemplate : ViewCell
	{
		Label FirstNameLabel;
		Label LastNameLabel;
		Label UsernameLabel;
		Button AddButton;

		public SearchUsersTemplate()
		{
			createElements();
			setBindings();
			setListeners();

			View = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10),
				Children =
				{
					new StackLayout
					{
						HorizontalOptions = LayoutOptions.StartAndExpand,
						Children =
						{
							new StackLayout
							{
								Orientation = StackOrientation.Horizontal,
								Children =
								{
									FirstNameLabel,
									LastNameLabel
								}
							},
							UsernameLabel
						}
					},
					AddButton
				}
			};
		}

		void createElements()
		{
			FirstNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			LastNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			UsernameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = Color.Gray,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			AddButton = new Button
			{
				Text = LocalizationRes.AddFriendButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button,
				MinimumWidthRequest = 50,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
		}

		void setBindings()
		{
			FirstNameLabel.SetBinding(Label.TextProperty, "first_name");
			LastNameLabel.SetBinding(Label.TextProperty, "last_name");
			UsernameLabel.SetBinding(Label.TextProperty, "username");
		}

		void setListeners()
		{
			AddButton.Clicked += (sender, e) =>
			{
				var user2 = this.BindingContext as UserSearchModel;

				var friend = new FriendPostModel
				{
					status = 1,
					user1 = App.CurrentUser.id,
					user2 = user2.id
				};

				if (App.RestWS.CreateFriend(friend))
				{
					AddButton.Text = LocalizationRes.FriendAddedLabel;
					AddButton.WidthRequest = 100;
					AddButton.BackgroundColor = Color.Transparent;
					AddButton.IsEnabled = false;
					AddButton.TextColor = ColorRes.Text;
				}
			};
		}
	}
}
