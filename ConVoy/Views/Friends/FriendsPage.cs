﻿using System;
using System.Collections.Generic;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class FriendsPage : ContentPage
	{
		List<FriendModel> Friends;

		ListView FriendsListView;

		public FriendsPage()
		{
			Title = LocalizationRes.FriendsPageTitle;

			if (Device.OS == TargetPlatform.iOS)
				Icon = "ic_tab_friends.png";

			getFriends();
			createElements();
			setMessagingCenter();

			BackgroundColor = ColorRes.Background;

			if (Friends.Count > 0)
				Content = FriendsListView;
			else
			{
				Content = new Label
				{
					Text = LocalizationRes.FriendsErrorAlert,
					FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
					FontFamily = FontRes.LabelFont,
					TextColor = ColorRes.Text,
					HorizontalTextAlignment = TextAlignment.Center,
					Margin = new Thickness(10),
					HorizontalOptions = LayoutOptions.CenterAndExpand,
					VerticalOptions = LayoutOptions.CenterAndExpand,
				};
			}
		}

		void getFriends()
		{
			Friends = App.RestWS.GetFriends();

			if (Friends == null)
				Friends = new List<FriendModel>();
		}

		void createElements()
		{
			FriendsListView = new ListView
			{
				ItemTemplate = new DataTemplate(typeof(FriendsTemplate)),
				ItemsSource = Friends,
				RowHeight = 70,
				BackgroundColor = Color.Transparent,
			};
		}

		void setMessagingCenter()
		{
			MessagingCenter.Subscribe<FriendsTemplate>(this, "RefreshFriendsList", (obj) =>
			{
				getFriends();
				FriendsListView.ItemsSource = Friends;
			});
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (FriendsListView != null)
			{
				getFriends();
				FriendsListView.ItemsSource = Friends;
			}
		}
	}
}
