﻿using System;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class FriendsTemplate : ViewCell
	{
		Label FirstNameLabel;
		Label LastNameLabel;
		Label UsernameLabel;
		Label StatusLabel;
		Button AcceptButton;

		MenuItem DeleteItem;

		StackLayout Stack;

		public FriendsTemplate()
		{
			setItems();
			createElements();
			createLayouts();
			setLayouts();
			setBindings();
			setListeners();

			View = Stack;
		}

		void setItems()
		{
			DeleteItem = new MenuItem
			{
				//Text = "Delete",
				IsDestructive = true
			};

			this.ContextActions.Add(DeleteItem);
		}

		void createElements()
		{
			FirstNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			LastNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			UsernameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = ColorRes.Text,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			StatusLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Micro, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = Color.Gray,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			AcceptButton = new Button
			{
				Text = LocalizationRes.AcceptFriendButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button,
				MinimumWidthRequest = 100,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
		}

		void createLayouts()
		{
			Stack = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10),
			};
		}

		void setLayouts()
		{
			Stack.Children.Add(new StackLayout
			{
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Children =
				{
					new StackLayout
					{
						Orientation = StackOrientation.Horizontal,
						Children =
						{
							FirstNameLabel,
							LastNameLabel
						}
					},
					UsernameLabel
				}
			});

		}

		void setBindings()
		{
			FirstNameLabel.SetBinding(Label.TextProperty, "FirstNameFriend");
			LastNameLabel.SetBinding(Label.TextProperty, "LastNameFriend");
			UsernameLabel.SetBinding(Label.TextProperty, "UsernameFriend");
			StatusLabel.SetBinding(Label.TextProperty, "StatusFriend");
			DeleteItem.SetBinding(MenuItem.TextProperty, "DeleteItemText");
		}

		void setListeners()
		{
			AcceptButton.Clicked += (sender, e) =>
			{
				var friend = this.BindingContext as FriendModel;

				App.RestWS.UpdateFriend(new FriendPostModel
				{
					id = friend.id,
					user1 = friend.user1.id,
					user2 = friend.user2.id,
					status = 2,
				});

				AcceptButton.IsVisible = false;
			};

			DeleteItem.Clicked += (sender, e) =>
			{
				var friend = this.BindingContext as FriendModel;

				App.RestWS.DeleteFriend(friend.id);

				MessagingCenter.Send(this, "RefreshFriendsList");
			};
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (this.BindingContext != null)
			{
				var friend = this.BindingContext as FriendModel;

				if (friend.user2.id == App.CurrentUser.id && friend.status == 1)
				{
					Stack.Children.Add(AcceptButton);
					//this.ContextActions.Clear();
				}
				else
					Stack.Children.Add(StatusLabel);
				
			}
		}

	}
}
