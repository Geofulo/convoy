﻿using System;
using System.Collections.Generic;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class SearchFriendsPage : ContentPage
	{
		SearchBar FriendsSearchBar;
		ListView FriendsListView;

		StackLayout Stack;

		public SearchFriendsPage()
		{
			Title = LocalizationRes.SearchFriendsPageTitle;

			createElements();
			createLayouts();
			setLayouts();
			setListeners();

			BackgroundColor = ColorRes.Background;

			Content = Stack;
		}

		void createElements()
		{
			FriendsSearchBar = new SearchBar
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(SearchBar)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			FriendsListView = new ListView
			{
				ItemTemplate = new DataTemplate(typeof(SearchUsersTemplate)),
				RowHeight = 70,
				BackgroundColor = Color.Transparent,
			};
		}

		void createLayouts()
		{
			Stack = new StackLayout();
		}

		void setLayouts()
		{
			Stack.Children.Add(FriendsSearchBar);
			Stack.Children.Add(FriendsListView);
		}

		void setListeners()
		{
			FriendsSearchBar.SearchButtonPressed += (sender, e) =>
			{
				List<UserSearchModel> users = App.RestWS.SearchFriends(FriendsSearchBar.Text);
				if (users != null)
					FriendsListView.ItemsSource = users;				
			};
		}
	}
}
