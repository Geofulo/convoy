﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace ConVoy
{
	public class MainPage : TabbedPage
	{
		ToolbarItem CreateItem;
		ToolbarItem SearchItem;

		ProfilePage ProfilePage;

		public MainPage()
		{
			Title = "ConVoy";

			setItems();
			createPages();
			setListeners();

			BackgroundColor = ColorRes.Background;
			NavigationPage.SetBackButtonTitle(this, "");

			if (Device.OS == TargetPlatform.Android)
			{
				BarBackgroundColor = ColorRes.NavBar;
				BarTextColor = ColorRes.Background;
			}
			if (Device.OS == TargetPlatform.iOS)
			{
				BarTextColor = ColorRes.NavBar;
			}

			Children.Add(ProfilePage);
			Children.Add(new FriendsPage());
			Children.Add(new HistorialPage());
		}

		void setItems()
		{
			CreateItem = new ToolbarItem
			{
				Text = "New",
				Icon = "ic_nav_add.png",
			};

			SearchItem = new ToolbarItem
			{
				Text = "Search",
				Icon = "ic_nav_new_user.png"
			};
		}

		void createPages()
		{
			ProfilePage = new ProfilePage();
		}

		void setListeners()
		{
			CreateItem.Clicked += (sender, e) => Navigation.PushAsync(new CreateConvoyPage());

			SearchItem.Clicked += (sender, e) => Navigation.PushAsync(new SearchFriendsPage());

			this.CurrentPageChanged += (sender, e) =>
			{
				if (this.CurrentPage.GetType() == typeof(ProfilePage))
				{
					if (ProfilePage.Convoy == null)
					{
						ToolbarItems.Clear();
						ToolbarItems.Add(CreateItem);
					}
					else
						ToolbarItems.Clear();
				}
				else if (this.CurrentPage.GetType() == typeof(FriendsPage))
				{
					ToolbarItems.Clear();
					ToolbarItems.Add(SearchItem);
				}
				else if (this.CurrentPage.GetType() == typeof(HistorialPage))
					ToolbarItems.Clear();
			};
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (this.CurrentPage.GetType() == typeof(ProfilePage))
			{
				if (ProfilePage != null && CreateItem != null)
				{
					if (ProfilePage.Convoy == null)
					{
						ToolbarItems.Clear();
						ToolbarItems.Add(CreateItem);
					}
					else
						ToolbarItems.Clear();
				}
			}
		}

	}
}
