﻿using System;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class SignupPage : ContentPage
	{
		Entry CorreoEntry;
		Entry PasswordEntry;
		Entry NombreEntry;
		Entry ApellidosEntry;

		Button RegistrarseButton;
		Button LoginButton;

		public SignupPage()
		{
			Title = "Signup";

			createElements();
			setListeners();

			NavigationPage.SetHasNavigationBar(this, false);
			BackgroundColor = ColorRes.Background;

			Content = new ScrollView
			{
				Content = new StackLayout
				{
					Spacing = 30,
					Padding = new Thickness(15, 60, 15, 15),
					Children =
					{
						new Image
						{
							Source = "logo_full.png",
							Margin = new Thickness(0, 0, 0, 20),
							HeightRequest = 60,
							HorizontalOptions = LayoutOptions.CenterAndExpand
						},
						new StackLayout {
							Spacing = 15,
							Children =
							{
								CorreoEntry,
								PasswordEntry,
								NombreEntry,
								ApellidosEntry,
								RegistrarseButton
							}
						},
						new StackLayout
						{
							Orientation = StackOrientation.Horizontal,
							Children =
							{
								new Label {
									Text = LocalizationRes.ToLoginLabel,
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
									FontFamily = FontRes.LabelFont,
									TextColor = ColorRes.Text,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								},
								LoginButton
							}
						}
					}
				}
			};
		}

		void createElements()
		{
			PasswordEntry = new Entry
			{
				Placeholder = LocalizationRes.PasswordEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				IsPassword = true,
				TextColor = ColorRes.Text,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			CorreoEntry = new Entry
			{
				Placeholder = LocalizationRes.EmailEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				Keyboard = Keyboard.Email,
				TextColor = ColorRes.Text,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			NombreEntry = new Entry
			{
				Placeholder = LocalizationRes.FirstNameEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			ApellidosEntry = new Entry
			{
				Placeholder = LocalizationRes.LastNameEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			RegistrarseButton = new Button
			{
				Text = LocalizationRes.SignupButton,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			LoginButton = new Button
			{
				Text = LocalizationRes.LoginButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button2,
			};

			if (Device.OS == TargetPlatform.iOS)
				LoginButton.WidthRequest += 90;
		}

		void setListeners()
		{
			RegistrarseButton.Clicked += (sender, e) =>
			{
				var password = PasswordEntry.Text;
				var correo = CorreoEntry.Text;
				var nombre = NombreEntry.Text;
				var apellidos = ApellidosEntry.Text;

				if (!string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(correo) && !string.IsNullOrEmpty(nombre))
				{

					var usuario = new UserModel
					{
						username = correo,
						password = password,
						email = correo,
						first_name = nombre,
						last_name = apellidos
					};

					if (App.RestWS.Signup(usuario))
					{
						if (App.RestWS.Login(correo, password))
						{
							App.RestWS.UpdateUsuario(new UserSearchModel
							{
								id = App.CurrentUser.id,
								email = App.CurrentUser.email,
								username = App.CurrentUser.email,
								first_name = nombre,
								last_name = apellidos
							});

							App.CurrentUser = App.RestWS.GetMe();

							Application.Current.MainPage = new NavigationPage(new MainPage())
							{
								BarBackgroundColor = ColorRes.NavBar,
								BarTextColor = ColorRes.Background,
							};
						}
						else
							DisplayAlert("", LocalizationRes.LoginErrorAlert, "OK");
					}
					else
						DisplayAlert("", LocalizationRes.SignupErrorAlert, "OK");
				}
				else
				{
					DisplayAlert("", LocalizationRes.FieldsErrorAlert, "OK");
				}
			};

			LoginButton.Clicked += (sender, e) => Navigation.PopAsync();
		}
	}
}
