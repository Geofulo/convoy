﻿using System;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class LoginPage : ContentPage
	{
		Entry UsernameEntry;
		Entry PasswordEntry;

		Button LoginButton;
		Button SignupButton;

		public LoginPage()
		{
			Title = "Login";

			createElements();
			setListeners();

			NavigationPage.SetHasNavigationBar(this, false);
			BackgroundColor = ColorRes.Background;

			Content = new ScrollView
			{
				Content = new StackLayout
				{
					Spacing = 30,
					Padding = new Thickness(15, 60, 15, 15),
					Children =
					{						
						new Image
						{
							Source = "logo_full.png",
							Margin = new Thickness(0, 0, 0, 20),
							HeightRequest = 60,
							HorizontalOptions = LayoutOptions.CenterAndExpand
						},
						new StackLayout {
							Spacing = 15,
							Children =
							{
								UsernameEntry,
								PasswordEntry,
								LoginButton
							}
						},
						new StackLayout
						{
							Orientation = StackOrientation.Horizontal,
							Children =
							{
								new Label {
									Text = LocalizationRes.ToSignupLabel,
									FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
									FontFamily = FontRes.LabelFont,
									TextColor = ColorRes.Text,
									VerticalOptions = LayoutOptions.CenterAndExpand,
								},
								SignupButton
							}
						}
					}
				}
			};
		}

		void createElements()
		{
			UsernameEntry = new Entry
			{
				Placeholder = LocalizationRes.EmailEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				Keyboard = Keyboard.Email,
				TextColor = ColorRes.Text,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			PasswordEntry = new Entry
			{
				Placeholder = LocalizationRes.PasswordEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				IsPassword = true,
				TextColor = ColorRes.Text,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			LoginButton = new Button
			{
				Text = LocalizationRes.LoginButton,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			SignupButton = new Button
			{
				Text = LocalizationRes.SignupButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button2,
			};
			if (Device.OS == TargetPlatform.iOS)
				SignupButton.WidthRequest += 90;
		}

		void setListeners()
		{
			LoginButton.Clicked += (sender, e) =>
			{
				var username = UsernameEntry.Text;
				var password = PasswordEntry.Text;

				if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
				{
					if (App.RestWS.Login(username, password))
					{
						//App.RestWS.RegistrarDevice();
						Application.Current.MainPage = new NavigationPage(new MainPage())
						{
							BarBackgroundColor = ColorRes.NavBar,
							BarTextColor = ColorRes.Background,
						};
					}
					else
						DisplayAlert("", LocalizationRes.LoginErrorAlert, "OK");
				}
				else 
					DisplayAlert("", LocalizationRes.FieldsErrorAlert, "OK");
			};

			SignupButton.Clicked += (sender, e) => Navigation.PushAsync(new SignupPage());
		}
	}
}
