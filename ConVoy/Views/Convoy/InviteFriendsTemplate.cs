﻿using System;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class InviteFriendsTemplate : ViewCell
	{
		Label FirstNameLabel;
		Label LastNameLabel;
		Label UsernameLabel;
		Button InviteButton;

		public InviteFriendsTemplate()
		{
			createElements();
			setBindings();
			setListeners();

			View = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10),
				Children =
				{
					new StackLayout
					{
						HorizontalOptions = LayoutOptions.StartAndExpand,
						Children =
						{
							new StackLayout
							{
								Orientation = StackOrientation.Horizontal,
								Children =
								{
									FirstNameLabel,
									LastNameLabel
								}
							},
							UsernameLabel
						}
					},
					InviteButton
				}
			};
		}

		void createElements()
		{
			FirstNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.EntryFont,
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			LastNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.EntryFont,
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			UsernameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			InviteButton = new Button
			{
				Text = LocalizationRes.InviteFriendButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button,
				MinimumWidthRequest = 80,
				HorizontalOptions = LayoutOptions.End,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			if (Device.OS == TargetPlatform.iOS)
				InviteButton.WidthRequest = 80;
		}

		void setBindings()
		{
			FirstNameLabel.SetBinding(Label.TextProperty, "FirstNameFriend");
			LastNameLabel.SetBinding(Label.TextProperty, "LastNameFriend");
			UsernameLabel.SetBinding(Label.TextProperty, "UsernameFriend");
		}

		void setListeners()
		{
			InviteButton.Clicked += (sender, e) =>
			{
				FriendModel friend = (FriendModel)this.BindingContext;

				var convoyer = new ConvoyerPostModel
				{
					status = 1,
					device_token = "0000",
				};

				if (friend.user2.id == App.CurrentUser.id)
					convoyer.user = friend.user1.id;
				else
					convoyer.user = friend.user2.id;

				MessagingCenter.Send(this, "InviteFriend", convoyer);

				InviteButton.IsEnabled = false;
			};
		}
	}
}
