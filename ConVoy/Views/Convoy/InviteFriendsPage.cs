﻿using System;
using System.Collections.Generic;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class InviteFriendsPage : ContentPage
	{
		ConvoyModel Convoy;
		List<FriendModel> Friends = new List<FriendModel>();
		List<ConvoyerModel> _convoyers = new List<ConvoyerModel>();

		ToolbarItem DoneItem;

		SearchBar UserSearch;
		ListView FriendsListView;

		public InviteFriendsPage(ConvoyModel Convoy)
		{
			this.Convoy = Convoy;

			Title = LocalizationRes.InviteFriendsPageTitle;

			setItems();
			getFriends();
			createElements();
			setListeners();
			setMessagingCenter();

			BackgroundColor = ColorRes.Background;
			NavigationPage.SetHasBackButton(this, false);

			Content = new StackLayout
			{
				Spacing = 0,
				Children =
				{
					UserSearch,
					FriendsListView
				}
			};
		}

		public InviteFriendsPage(ConvoyModel Convoy, List<ConvoyerModel> convoyers)
		{
			this.Convoy = Convoy;
			this._convoyers = convoyers;

			Title = LocalizationRes.InviteFriendsPageTitle;

			getFriends();
			createElements();
			setMessagingCenter();

			BackgroundColor = ColorRes.Background;

			Content = new StackLayout
			{
				Spacing = 0,
				Children =
				{
					UserSearch,
					FriendsListView
				}
			};
		}

		void setItems()
		{
			DoneItem = new ToolbarItem
			{
				Text = "Done",
				Icon = "ic_nav_done.png"
			};

			ToolbarItems.Add(DoneItem);
		}

		void getFriends()
		{
			var friends = App.RestWS.GetFriends();

			if (friends == null)
				friends = new List<FriendModel>();
			
			foreach (var f in friends)
			{
				if (f.status == 2)
				{
					if (_convoyers.Count > 1)
					{
						bool isAdd = false;
						foreach (var c in _convoyers)
						{
							if (!isAdd)
							{
								if (c.User.id != f.user1.id && c.User.id != f.user2.id)
								{
									Friends.Add(f);
									isAdd = true;
								}
							}
						}
					}
					else
						Friends.Add(f);
				}
			}
		}

		void createElements()
		{
			UserSearch = new SearchBar
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(SearchBar)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HorizontalOptions = LayoutOptions.FillAndExpand
			};

			FriendsListView = new ListView
			{
				ItemTemplate = new DataTemplate(typeof(InviteFriendsTemplate)),
				ItemsSource = Friends,
				RowHeight = 70,
				BackgroundColor = Color.Transparent,
			};
		}

		void setListeners()
		{
			DoneItem.Clicked += (sender, e) =>
			{
				Navigation.PopToRootAsync();
			};
		}

		void setMessagingCenter()
		{
			MessagingCenter.Subscribe<InviteFriendsTemplate, ConvoyerPostModel>(this, "InviteFriend", (arg1, convoyer) => 
			{
				convoyer.convoy = Convoy.id;
				if (!App.RestWS.CreateConvoyer(convoyer))
					DisplayAlert("", LocalizationRes.InviteFriendErrorAlert, "OK");
			});
		}
	}
}
