﻿using System;
using System.Threading.Tasks;
using ConVoy.Resources.Localization;
using Plugin.Geolocator;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace ConVoy
{
	public class CreateConvoyPage : ContentPage
	{
		ToolbarItem NextItem;

		Entry NameEntry;
		SearchBar DestinationSearch;
		Map DestinationMap;

		RelativeLayout RelativeMap;
		StackLayout Stack;

		public CreateConvoyPage()
		{
			Title = LocalizationRes.CreateConvoyPageTitle;

			createItems();
			createElements();
			createLayouts();
			setLayouts();
			setListeners();

			BackgroundColor = ColorRes.Background;
			NavigationPage.SetBackButtonTitle(this, "");

			Content = Stack;
		}

		void createItems()
		{
			NextItem = new ToolbarItem
			{
				Text = "Next",
				Icon = "ic_nav_next.png",
			};

			ToolbarItems.Add(NextItem);
		}

		void createElements()
		{
			NameEntry = new Entry
			{
				Placeholder = LocalizationRes.ConvoyNameEntry,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				Margin = new Thickness(10),
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			DestinationSearch = new SearchBar
			{
				Placeholder = LocalizationRes.SearchDestinationEntry,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Entry)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};

			DestinationSearch.SearchCommand = new Command(async () => 
			{ 
				var position = await new Geocoder().GetPositionsForAddressAsync(DestinationSearch.Text);
				if (position != null)
				{
					foreach (var pos in position)
					{
						Device.BeginInvokeOnMainThread(() =>
						{
							DestinationMap.MoveToRegion(MapSpan.FromCenterAndRadius(pos, Distance.FromMeters(1000)));
						});
					}
				}
				else
					await DisplayAlert("", LocalizationRes.PlaceSearchErrorAlert, "OK");
			});

			DestinationMap = new Map
			{
				MapType = MapType.Street,
				HasScrollEnabled = true,
				HasZoomEnabled = true,
				IsShowingUser = true,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.Fill,
			};

			Task.Run(async () => { 
				var position = await CrossGeolocator.Current.GetPositionAsync(100);
				if (position != null)
				{
					Device.BeginInvokeOnMainThread(() =>
					{						
						DestinationMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromMeters(100)));
					});
				}
			});
		}

		void createLayouts()
		{
			RelativeMap = new RelativeLayout
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			Stack = new StackLayout
			{
				Spacing = 0,
			};
		}

		void setLayouts()
		{
			RelativeMap.Children.Add(
				DestinationMap, 
				Constraint.Constant(0),
				Constraint.Constant(0),
				Constraint.RelativeToParent((arg) => arg.Width),
				Constraint.RelativeToParent((arg) => arg.Height - Device.OnPlatform<int>(0, 58, 0))
			);

			RelativeMap.Children.Add(
				new Image
				{
					Source = "ic_pin.png",
					WidthRequest = 40,
					HeightRequest = 40,
					Aspect = Aspect.AspectFit,
				},
				Constraint.RelativeToView(DestinationMap, (arg1, arg2) => (arg2.Width / 2) - 20),
				//Constraint.RelativeToView(DestinationMap, (arg1, arg2) => (arg2.Height / 2) - 20 - NameEntry.Height - DestinationSearch.Height)
				//Constraint.RelativeToParent((arg) => arg.Width / 2 - 20),
				Constraint.RelativeToParent((arg) => (arg.Height / 2) - 20 - 56)
			);

			Stack.Children.Add(NameEntry);
			Stack.Children.Add(DestinationSearch);
			Stack.Children.Add(RelativeMap);
		}

		void setListeners()
		{
			NextItem.Clicked += (sender, e) => 
			{
				string name = NameEntry.Text;
				var latitude = DestinationMap.VisibleRegion.Center.Latitude;
				var longitude = DestinationMap.VisibleRegion.Center.Longitude;

				if (!string.IsNullOrEmpty(name))
				{
					var convoy = new ConvoyModel
					{
						user = App.CurrentUser.id,
						name = name,
						latitude_destination = latitude,
						longitude_destination = longitude,
						status = 1,
					};

					var convoyCreated = App.RestWS.CreateConvoy(convoy);
					if (convoyCreated != null)
					{
						App.RestWS.CreateConvoyer(new ConvoyerPostModel
						{
							convoy = convoyCreated.id,
							user = App.CurrentUser.id,
							status = 2,
							device_token = "0000"
						});

						Navigation.PushAsync(new InviteFriendsPage(convoyCreated));
					}
					else
						DisplayAlert("", LocalizationRes.CreateConvoyErrorAlert, "OK");
				}
				else
					DisplayAlert("Error", LocalizationRes.FieldsCreateConvoyErrorAlert, "OK");
			};
		}
	}
}
