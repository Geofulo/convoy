﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ConVoy.Resources.Localization;
using Plugin.Geolocator;
using Xamarin.Forms;
//using Xamarin.Forms.Maps;
using Xamarin.Forms.GoogleMaps;

namespace ConVoy
{
	public class ConvoyLivePage : ContentPage
	{
		ConvoyModel Convoy;
		List<ConvoyerModel> Convoyers = new List<ConvoyerModel>();
		ConvoyerModel CurrentConvoyer = null;

		Map ConvoyMapa;
		List<Pin> ConvoyersPin = new List<Pin>();

		ToolbarItem ConvoyersItem;

		Button StartButton;
		Button FinishButton;

		RelativeLayout Relative;

		CancellationTokenSource _cancelToken;

		public ConvoyLivePage(ConvoyModel Convoy)
		{
			this.Convoy = Convoy;

			Title = "Convoy " + Convoy.name;

			setItems();
			createElements();
			createLayouts();
			setLayouts();

			setPins();

			if (Convoy.status == 2)
			{
				setUserUpdatePosition();
				setConvoyersUpdatePosition();
			}

			if (Convoy.user == App.CurrentUser.id)
				setListeners();

			NavigationPage.SetBackButtonTitle(this, "");

			Content = Relative;
		}

		void setItems()
		{
			if (Convoy.user == App.CurrentUser.id)
			{
				ConvoyersItem = new ToolbarItem
				{
					Text = "Convoyers",
					Icon = "ic_nav_convoyers.png",
				};

				ToolbarItems.Add(ConvoyersItem);
			}
		}

		void getConvoyers()
		{
			Convoyers = App.RestWS.GetConvoyers(Convoy.id);

			if (Convoyers == null)
				Convoyers = new List<ConvoyerModel>();

			foreach (var c in Convoyers)
			{
				if (c.User.id == App.CurrentUser.id)
					CurrentConvoyer = c;
			}
		}

		void createElements()
		{
			ConvoyMapa = new Map
			{
				MapType = MapType.Street,
				IsShowingUser = true,
				HasZoomEnabled = true,
				HasScrollEnabled = true,
				WidthRequest = App.ScreenWidth,
				HeightRequest = Device.OnPlatform<int>(App.ScreenHeight - 60, App.ScreenHeight - 80, App.ScreenHeight),
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			StartButton = new Button
			{
				Text = LocalizationRes.StartConvoyButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = ColorRes.NavBar,
				TextColor = ColorRes.Background,
				BorderRadius = 30,
				MinimumWidthRequest = 60,
				HeightRequest = 60,
			};
			FinishButton = new Button
			{
				Text = LocalizationRes.FinishConvoyButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				FontAttributes = FontAttributes.Bold,
				BackgroundColor = ColorRes.Button,
				TextColor = ColorRes.Background,
				BorderRadius = 30,
				MinimumWidthRequest = 60,
				HeightRequest = 60,
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				StartButton.WidthRequest = 60;
				FinishButton.WidthRequest = 60;
			}

			Task.Run(async () =>
			{
				if (CrossGeolocator.Current.IsGeolocationEnabled && CrossGeolocator.Current.IsGeolocationAvailable)
				{
					var position = await CrossGeolocator.Current.GetPositionAsync();

					if (position != null)
					{
						Device.BeginInvokeOnMainThread(() =>
						{

							ConvoyMapa.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(position.Latitude, position.Longitude), Distance.FromKilometers(3)));
							//ConvoyMapa.RouteCoordinates.Add(new Position(position.Latitude, position.Longitude));
							//ConvoyMapa.RouteCoordinates.Add(new Position(Convoy.latitude_destination, Convoy.longitude_destination));
						});
					}
				}
			});

			_cancelToken = new CancellationTokenSource();
		}

		void createLayouts()
		{
			Relative = new RelativeLayout();
		}

		void setPins()
		{
			Device.BeginInvokeOnMainThread(() => ConvoyMapa.Pins.Clear());

			if (Convoy.status == 2)
			{
				if (Convoyers != null)
				{
					foreach (var c in Convoyers)
					{
						if (c.User.id != App.CurrentUser.id && c.latitude != 0.0 && c.longitude != 0.0)
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								ConvoyMapa.Pins.Add(new Pin
								{
									Label = c.User.first_name + " " + c.User.last_name,
									Position = new Position(c.latitude, c.longitude),
									Type = PinType.Generic,
									Address = c.User.first_name + " " + c.User.last_name,
									Icon = BitmapDescriptorFactory.FromView(new Image
									{
										Source = "ic_pin_blue.png",
										WidthRequest = 25,
										HeightRequest = 30,
									}),
								});
							});

							//ConvoyMapa.RouteCoordinates.Add(new Position(c.latitude, c.longitude));
						}
					}
				}
			}

			Device.BeginInvokeOnMainThread(() =>
			{
				ConvoyMapa.Pins.Add(new Pin
				{
					Label = "Convoy " + Convoy.name,
					Position = new Position(Convoy.latitude_destination, Convoy.longitude_destination),
					Type = PinType.Generic,
					Address = Convoy.name,
					Icon = BitmapDescriptorFactory.FromView(new Image
					{
						Source = "ic_pin_red.png",
						WidthRequest = 25,
						HeightRequest = 30,
					}),
				});
			});
		}

		void setUserUpdatePosition()
		{
			Task.Run(async () =>
			{
				if (CrossGeolocator.Current.IsGeolocationEnabled && CrossGeolocator.Current.IsGeolocationAvailable)
				{
					CrossGeolocator.Current.AllowsBackgroundUpdates = true;

					if (!_cancelToken.IsCancellationRequested)
					{
						var listening = await CrossGeolocator.Current.StartListeningAsync(500, 100);

						if (listening)
						{
							CrossGeolocator.Current.PositionChanged += (sender, e) =>
							{
								if (e.Position != null && CurrentConvoyer != null)
								{
									CurrentConvoyer.latitude = e.Position.Latitude;
									CurrentConvoyer.longitude = e.Position.Longitude;

									App.RestWS.UpdateConvoyer(new ConvoyerPostModel
									{
										id = CurrentConvoyer.id,
										convoy = CurrentConvoyer.convoy,
										user = CurrentConvoyer.User.id,
										latitude = e.Position.Latitude,
										longitude = e.Position.Longitude,
										device_token = CurrentConvoyer.device_token,
										status = CurrentConvoyer.status
									});

									//ConvoyMapa.RouteCoordinates.Clear();
									//ConvoyMapa.RouteCoordinates.Insert(0, new Position(e.Position.Latitude, e.Position.Longitude));
									//ConvoyMapa.RouteCoordinates.Insert(1, new Position(Convoy.latitude_destination, Convoy.longitude_destination));
									//ConvoyMapa.RouteCoordinates.Add(new Position(e.Position.Latitude, e.Position.Longitude));
									//ConvoyMapa.RouteCoordinates.Add(new Position(Convoy.latitude_destination, Convoy.longitude_destination));
								}
							};
						}
					}
					else
						await CrossGeolocator.Current.StopListeningAsync();
				}
			}, _cancelToken.Token);
		}

		void setConvoyersUpdatePosition()
		{
			Task.Run(async () =>
			{
				while (!this._cancelToken.IsCancellationRequested)
				{
					getConvoyers();

					if (Convoyers.Count == ConvoyMapa.Pins.Count - 1)
					{
						for (int i = 0; i < Convoyers.Count; i++)
						{
							Device.BeginInvokeOnMainThread(() => ConvoyMapa.Pins[i].Position = new Position(Convoyers[i].latitude, Convoyers[i].longitude));
						}
					}
					else
					{
						setPins();
					}

					await Task.Delay(5000);
				}
			}, _cancelToken.Token);
		}

		void setLayouts()
		{
			Relative.Children.Clear();

			Relative.Children.Add(
				ConvoyMapa,
				Constraint.Constant(0),
				Constraint.Constant(0)
			);

			if (Convoy.status == 1)
			{
				Relative.Children.Add(
					StartButton,
					Constraint.RelativeToParent((arg) => (arg.Width / 2) - (FinishButton.MinimumWidthRequest / 2)),
					Constraint.RelativeToParent((arg) => arg.Height - FinishButton.HeightRequest - 15)
				);
			}
			else if (Convoy.status == 2)
			{
				Relative.Children.Add(
					FinishButton,
					Constraint.RelativeToParent((arg) => (arg.Width / 2) - (FinishButton.Width / 2)),
					Constraint.RelativeToParent((arg) => arg.Height - FinishButton.HeightRequest - 15)
				);
			}
		}

		void setListeners()
		{
			ConvoyersItem.Clicked += (sender, e) =>
			{
				Navigation.PushAsync(new ConvoyersPage(Convoy));
			};

			StartButton.Clicked += (sender, e) =>
			{
				Convoy.status = 2;

				if (App.RestWS.UpdateConvoy(Convoy))
				{
					setLayouts();
					setUserUpdatePosition();
					setPins();
					setConvoyersUpdatePosition();
				}
				else
					DisplayAlert("", LocalizationRes.StartConvoyErrorAlert, "OK");
			};

			FinishButton.Clicked += (sender, e) =>
			{
				Convoy.status = 3;

				if (App.RestWS.UpdateConvoy(Convoy))
				{
					Navigation.PopToRootAsync();
				}
				else
					DisplayAlert("", LocalizationRes.FinishConvoyErrorAlert, "OK");
			};
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();

			_cancelToken.Cancel();
			CrossGeolocator.Current.StopListeningAsync();
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
		}

	}
}
