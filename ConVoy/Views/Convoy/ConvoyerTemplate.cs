﻿using System;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class ConvoyerTemplate : ViewCell
	{
		Label FirstNameLabel;
		Label LastNameLabel;
		Label UsernameLabel;
		Label StatusLabel;

		MenuItem DeleteItem;

		public ConvoyerTemplate()
		{
			createElements();
			setBindings();
			setListeners();

			View = new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10),
				Children =
				{
					new StackLayout
					{
						HorizontalOptions = LayoutOptions.StartAndExpand,
						Children =
						{
							new StackLayout
							{
								Orientation = StackOrientation.Horizontal,
								Children =
								{
									FirstNameLabel,
									LastNameLabel
								}
							},
							UsernameLabel
						}
					},
					StatusLabel
				}
			};
		}

		void createElements()
		{
			FirstNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			LastNameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};
			UsernameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = ColorRes.Text,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			StatusLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = Color.Gray,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand
			};

			DeleteItem = new MenuItem
			{
				Text = LocalizationRes.DeleteItem,
				IsDestructive = true,
			};

			ContextActions.Add(DeleteItem);
		}

		void setBindings()
		{
			FirstNameLabel.SetBinding(Label.TextProperty, "FirstNameConvoyer");
			LastNameLabel.SetBinding(Label.TextProperty, "LastNameConvoyer");
			UsernameLabel.SetBinding(Label.TextProperty, "UsernameConvoyer");
			StatusLabel.SetBinding(Label.TextProperty, "StatusConvoyer");
		}

		void setListeners()
		{
			DeleteItem.Clicked += (sender, e) =>
			{
				var convoyer = this.BindingContext as ConvoyerModel;
				if (App.RestWS.DeleteConvoyer(convoyer.id))
					MessagingCenter.Send(this, "DeleteConvoyer");
				else
					Application.Current.MainPage.DisplayAlert("Error", LocalizationRes.DeleteConvoyerErrorAlert, "OK");
			};
		}
	}
}
