﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace ConVoy
{
	public class ConvoyersPage : ContentPage
	{
		ConvoyModel Convoy;
		List<ConvoyerModel> Convoyers = new List<ConvoyerModel>();

		ListView ConvoyersListView;

		ToolbarItem InviteItem;

		public ConvoyersPage(ConvoyModel Convoy)
		{
			this.Convoy = Convoy;

			Title = "Convoyers";

			setItems();
			getConvoyers();
			createElements();
			setMessagingCenter();
			setListeners();

			NavigationPage.SetBackButtonTitle(this, "");
			BackgroundColor = ColorRes.Background;

			Content = ConvoyersListView;
		}

		void setItems()
		{
			InviteItem = new ToolbarItem
			{
				Text = "Invite",
				Icon = "ic_nav_add.png"
			};

			ToolbarItems.Add(InviteItem);
		}

		void getConvoyers()
		{
			var convoyers = App.RestWS.GetConvoyers(Convoy.id);

			if (convoyers == null)
				convoyers = new List<ConvoyerModel>();

			Convoyers.Clear();

			foreach (var c in convoyers)
			{
				if (c.User.id != App.CurrentUser.id)
					Convoyers.Add(c);
			}
		}

		void createElements()
		{
			ConvoyersListView = new ListView
			{
				ItemsSource = Convoyers,
				ItemTemplate = new DataTemplate(typeof(ConvoyerTemplate)),
				RowHeight = 70,
				BackgroundColor = Color.Transparent,
				//IsPullToRefreshEnabled = true,
			};
		}

		void setMessagingCenter()
		{
			MessagingCenter.Subscribe<ConvoyerTemplate>(this, "DeleteConvoyer", (obj) =>
			{
				getConvoyers();
				ConvoyersListView.ItemsSource = null;
				ConvoyersListView.ItemsSource = Convoyers;
			});
		}

		void setListeners()
		{
			InviteItem.Clicked += (sender, e) => Navigation.PushAsync(new InviteFriendsPage(Convoy, Convoyers));
		}

		//protected override void OnAppearing()
		//{
		//	base.OnAppearing();

		//	if (ConvoyersListView != null)
		//	{
		//		getConvoyers();
		//		createElements();
		//		ConvoyersListView.ItemsSource = null;
		//		ConvoyersListView.ItemsSource = Convoyers;
		//	}
		//}
	}
}
