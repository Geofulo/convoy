﻿using System;
using System.Collections.Generic;
using ConVoy.Resources.Localization;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace ConVoy
{
	public class ProfilePage : ContentPage
	{
		public ConvoyModel Convoy = null;
		List<ConvoyerModel> Convoyers = new List<ConvoyerModel>();
		ConvoyerModel CurrentConvoyer = null;

		Label FirstNameLabel;
		Label LastNameLabel;
		Label UsernameLabel;
		Button EditButton;

		Button CancelConvoyButton;
		Button AcceptConvoyButton;

		RelativeLayout RelativeMap;

		Button LogoutButton;

		StackLayout ConvoyStack;
		StackLayout Stack;

		TapGestureRecognizer TapGestureMap = new TapGestureRecognizer();

		public ProfilePage()
		{
			Title = LocalizationRes.ProfilePageTitle;

			if(Device.OS == TargetPlatform.iOS)
				Icon = "ic_tab_profile.png";

			getConvoy();
			getConvoyers();
			createElements();
			createLayouts();
			//setLayouts();
			setConvoyView();
			setBindings();
			setListeners();

			BackgroundColor = ColorRes.Background;
			NavigationPage.SetBackButtonTitle(this, "");

			Content = Stack;
		}

		void getConvoy()
		{
			Convoy = App.RestWS.GetCurrentConvoy();
		}

		void getConvoyers()
		{
			if (Convoy != null)
				Convoyers = App.RestWS.GetConvoyers(Convoy.id);

			if (Convoyers == null)
				Convoyers = new List<ConvoyerModel>();

			foreach (var c in Convoyers)
			{
				if (c.User.id == App.CurrentUser.id)
					CurrentConvoyer = c;
			}
		}

		void createElements()
		{
			FirstNameLabel = new Label
			{
				Text = App.CurrentUser.first_name,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			LastNameLabel = new Label
			{
				Text = App.CurrentUser.last_name,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			UsernameLabel = new Label
			{
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				FontFamily = FontRes.LabelFont,
				TextColor = ColorRes.Text,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};

			EditButton = new Button
			{
				Text = LocalizationRes.EditButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button2,
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.StartAndExpand,
			};
			if (Device.OS == TargetPlatform.iOS)
				EditButton.WidthRequest += 60;

			CancelConvoyButton = new Button
			{
				Text = LocalizationRes.CancelButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button2,
				BorderRadius = 0,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
			AcceptConvoyButton = new Button
			{
				Text = LocalizationRes.AcceptButton,
				FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Button)),
				FontFamily = FontRes.ButtonFont,
				FontAttributes = FontAttributes.Bold,
				TextColor = ColorRes.Background,
				BackgroundColor = ColorRes.Button,
				BorderRadius = 0,
				HeightRequest = 50,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};

			LogoutButton = new Button
			{
				Image = "ic_sign_out.png",
				BackgroundColor = Color.Transparent,
				BorderColor = Color.Transparent,
				BorderWidth = 0,
				WidthRequest = Device.OnPlatform<int>(25, 45, 25),
				HeightRequest = Device.OnPlatform<int>(25, 45, 25),
				Margin = new Thickness(0, 0, 15, 15),
				HorizontalOptions = LayoutOptions.EndAndExpand,
				VerticalOptions = LayoutOptions.EndAndExpand,
			};
		}

		void createLayouts()
		{
			ConvoyStack = new StackLayout();

			Stack = new StackLayout
			{
				Padding = new Thickness(10),
			};
		}

		void setLayouts()
		{
			Stack.Children.Add(new StackLayout
			{
				Orientation = StackOrientation.Horizontal,
				Children =
				{
					new StackLayout
					{
						Children =
						{
							new StackLayout
							{
								Orientation = StackOrientation.Horizontal,
								Children =
								{
									FirstNameLabel,
									LastNameLabel
								}
							},
							UsernameLabel
						}
					},
					EditButton
				}
			});
		}

		void setConvoyView()
		{
			if (Stack != null)
			{
				if (Convoy != null)
				{
					var convoyMap = new MapCustom
					{
						IsShowingUser = true,
						IsEnabled = false,
						HeightRequest = 200,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						HasScrollEnabled = false,
					};

					convoyMap.Pins.Add(new Pin
					{
						Position = new Position(Convoy.latitude_destination, Convoy.longitude_destination),
						Address = Convoy.name,
						Label = Convoy.name,
						Type = PinType.Generic,
						Icon = BitmapDescriptorFactory.FromView(new Image
						{
							Source = "ic_pin_red.png",
							WidthRequest = 25,
							HeightRequest = 30,
						}),
					});
						
					convoyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(Convoy.latitude_destination, Convoy.longitude_destination), Distance.FromKilometers(10)));


					RelativeMap = new RelativeLayout
					{
						Margin = new Thickness(0, 50, 0, 0),
						WidthRequest = App.ScreenWidth,
						HeightRequest = 250,
						IsClippedToBounds = true,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.FillAndExpand,
					};

					if (CurrentConvoyer.status == 1)
						RelativeMap.HeightRequest = 300;


					RelativeMap.Children.Clear();

					RelativeMap.Children.Add(
						new ContentViewCustom
						{
							CornerRadius = 15,
							IsClippedToBounds = true,
							HeightRequest = RelativeMap.HeightRequest,
							WidthRequest = App.ScreenWidth - 20,
							//Opacity = 0.6,
							Content = convoyMap,							
						},
						Constraint.Constant(0),
						Constraint.Constant(0),
						Constraint.RelativeToParent((arg) => arg.Width),
						Constraint.Constant(RelativeMap.HeightRequest)
					);

					var titleConvoy = new Label
					{
						Text = Convoy.name,
						FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
						FontFamily = FontRes.LabelFont,
						FontAttributes = FontAttributes.Bold,
						TextColor = ColorRes.Background,
						BackgroundColor = ColorRes.NavBar,
						HeightRequest = 50,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					};

					var titleConvoyContent = new ContentViewCustom
					{
						CornerRadius = 15,
						IsClippedToBounds = true,
						HeightRequest = RelativeMap.HeightRequest,
						WidthRequest = App.ScreenWidth - 20,
						BackgroundColor = Color.Transparent,
						Content = new StackLayout
						{
							Children =
							{
								titleConvoy
							}
						},
					};

					if (CurrentConvoyer.status == 2)
						titleConvoyContent.GestureRecognizers.Add(TapGestureMap);

					RelativeMap.Children.Add(
						titleConvoyContent,
						Constraint.Constant(0),
						Constraint.Constant(0)
					);

					if (CurrentConvoyer.status == 1)
					{
						titleConvoy.Text = String.Format(LocalizationRes.ConvoyViewTitleLabel, Convoy.name);

						RelativeMap.Children.Add(
								new ContentViewCustom
								{
									CornerRadius = 15,
									IsClippedToBounds = true,
									BackgroundColor = Color.Transparent,
									HeightRequest = RelativeMap.HeightRequest,
									//HeightRequest = 40,
									WidthRequest = App.ScreenWidth - 20,
									Content = new StackLayout
									{
										Orientation = StackOrientation.Horizontal,
										Spacing = 0,
										VerticalOptions = LayoutOptions.EndAndExpand,
										Children =
										{
											CancelConvoyButton,
											AcceptConvoyButton
										}
									},
								},
								Constraint.Constant(0),
								Constraint.Constant(0)
							);
					}

					//if (CurrentConvoyer.status == 2)  // for iOS
					//{
					//	RelativeMap.GestureRecognizers.Clear();
					//	RelativeMap.GestureRecognizers.Add(TapGestureMap);
					//}

					Stack.Children.Clear();
					setLayouts();
					Stack.Children.Add(RelativeMap);
					Stack.Children.Add(LogoutButton);
				}
				else
				{
					Stack.Children.Clear();
					setLayouts();
					Stack.Children.Add(LogoutButton);
				}
			}
		}

		void setBindings()
		{
			FirstNameLabel.BindingContext = App.CurrentUser;
			FirstNameLabel.SetBinding(Label.TextProperty, "first_name");
			LastNameLabel.BindingContext = App.CurrentUser;
			LastNameLabel.SetBinding(Label.TextProperty, "last_name");
			UsernameLabel.BindingContext = App.CurrentUser;
			UsernameLabel.SetBinding(Label.TextProperty, "username");
		}

		void setListeners()
		{
			EditButton.Clicked += (sender, e) => Navigation.PushAsync(new EditPagePage());

			TapGestureMap.Tapped += (sender, e) =>
			{
				if (Convoy != null)
					Navigation.PushAsync(new ConvoyLivePage(Convoy));
				else
				{
					getConvoy();
					DisplayAlert("", LocalizationRes.TapConvoyViewErrorAlert, "OK");
				}
			};

			CancelConvoyButton.Clicked += (sender, e) =>
			{
				if (App.RestWS.DeleteConvoyer(CurrentConvoyer.id))
				{
					Stack.Children.Clear();
					setLayouts();
				}
				else
					DisplayAlert("", LocalizationRes.ConvoyViewCancelErrorAlert, "OK");
			};

			AcceptConvoyButton.Clicked += (sender, e) =>
			{
				CurrentConvoyer.status = 2;

				foreach (var c in Convoyers)
				{
					if (c.id == CurrentConvoyer.id)
						c.status = 2;
				}

				var con = new ConvoyerPostModel
				{
					id = CurrentConvoyer.id,
					convoy = CurrentConvoyer.convoy,
					user = CurrentConvoyer.User.id,
					status = 2,
					latitude = CurrentConvoyer.latitude,
					longitude = CurrentConvoyer.longitude,
					device_token = CurrentConvoyer.device_token
				};

				if (App.RestWS.UpdateConvoyer(con))
				{
					setConvoyView();
				}
				else
					DisplayAlert("", LocalizationRes.ConvoyViewAcceptErrorAlert, "OK");
			};

			LogoutButton.Clicked += (sender, e) =>
			{
				if (App.RestWS.Logout())
				{
					App.Current.MainPage = new NavigationPage(new LoginPage())
					{
						BarBackgroundColor = ColorRes.NavBar,
						BarTextColor = ColorRes.Background,
					};
				}
				else
					DisplayAlert("", LocalizationRes.LogoutErrorAlert, "OK");
			};
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			getConvoy();
			getConvoyers();
			setConvoyView();
			setBindings();
		}
	}
}
