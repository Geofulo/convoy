﻿using System;
using ConVoy.Resources.Localization;
using Xamarin.Forms;

namespace ConVoy
{
	public class EditPagePage : ContentPage
	{
		ToolbarItem SaveItem;

		Entry FirstNameEntry;
		Entry LastNameEntry;
		Entry UsernameEntry;

		public EditPagePage()
		{
			Title = LocalizationRes.EditProfilePageTitle;

			createItems();
			createElements();
			setListeners();

			BackgroundColor = ColorRes.Background;
			NavigationPage.SetBackButtonTitle(this, "");

			Content = new StackLayout
			{
				Padding = new Thickness(10),
				Spacing = 15,
				Children =
				{
					new StackLayout
					{
						Children =
						{
							new Label
							{
								Text = LocalizationRes.FirstNameLabel,
								FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
								FontFamily = FontRes.LabelFont,
								TextColor = Color.Gray,
							},
							FirstNameEntry
						}
					},
					new StackLayout
					{
						Children =
						{
							new Label
							{
								Text = LocalizationRes.LastNameLabel,
								FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
								FontFamily = FontRes.LabelFont,
								TextColor = Color.Gray,
							},
							LastNameEntry
						}
					},
					new StackLayout
					{
						Children =
						{
							new Label
							{
								Text = LocalizationRes.EmailLabel,
								FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
								FontFamily = FontRes.LabelFont,
								TextColor = Color.Gray,
							},
							UsernameEntry
						}
					}
				}
			};
		}

		void createItems()
		{
			SaveItem = new ToolbarItem
			{
				Text = "Save",
				Icon = "ic_nav_done.png",
			};

			ToolbarItems.Add(SaveItem);
		}

		void createElements()
		{
			FirstNameEntry = new Entry
			{
				Placeholder = LocalizationRes.FirstNameEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			if (!string.IsNullOrEmpty(App.CurrentUser.first_name))
				FirstNameEntry.Text = App.CurrentUser.first_name;

			LastNameEntry = new Entry
			{
				Placeholder = LocalizationRes.LastNameEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			if (!string.IsNullOrEmpty(App.CurrentUser.last_name))
				LastNameEntry.Text = App.CurrentUser.last_name;

			UsernameEntry = new Entry
			{
				Placeholder = LocalizationRes.EmailEntry,
				FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
				FontFamily = FontRes.EntryFont,
				TextColor = ColorRes.Text,
				HeightRequest = 40,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
			};
			if (!string.IsNullOrEmpty(App.CurrentUser.username))
				UsernameEntry.Text = App.CurrentUser.username;
		}

		void setListeners()
		{
			SaveItem.Clicked += (sender, e) =>
			{
				var first_name = FirstNameEntry.Text;
				var last_name = LastNameEntry.Text;
				var username = UsernameEntry.Text;

				if (!string.IsNullOrEmpty(first_name) && !string.IsNullOrEmpty(last_name) && !string.IsNullOrEmpty(username))
				{
					var user = new UserSearchModel
					{
						id = App.CurrentUser.id,
						first_name = first_name,
						last_name = last_name,
						username = username,
						email = username
					};

					if (App.RestWS.UpdateUsuario(user))
					{
						App.CurrentUser = App.RestWS.GetMe();
						Navigation.PopAsync();
					}
				}
				else
					DisplayAlert("", LocalizationRes.EditProfileErrorAlert, "OK");				
			};
		}
	}
}
