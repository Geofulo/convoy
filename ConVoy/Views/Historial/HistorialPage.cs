﻿using System;
using System.Collections.Generic;
using ConVoy.Resources.Localization;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace ConVoy
{
	public class HistorialPage : ContentPage
	{
		List<ConvoyModel> Convoys = new List<ConvoyModel>();

		List<RelativeLayout> ConvoyViews = new List<RelativeLayout>();

		StackLayout Stack;
		ScrollView Scroll;

		public HistorialPage()
		{
			Title = LocalizationRes.HistorialPageTitle;

			if (Device.OS == TargetPlatform.iOS)
				Icon = "ic_tab_historial.png";

			getConvoys();
			createElements();
			createLayouts();
			setLayouts();

			BackgroundColor = ColorRes.Background;

			Content = Scroll;
		}

		void getConvoys()
		{
			Convoys = App.RestWS.GetHistorial();

			if (Convoys == null)
				Convoys = new List<ConvoyModel>();
		}

		void createElements()
		{
			foreach (var c in Convoys)
			{
				var convoyMap = new MapCustom
				{
					IsShowingUser = true,
					IsEnabled = false,
					HeightRequest = 150,
					HorizontalOptions = LayoutOptions.FillAndExpand,
				};
				convoyMap.Pins.Add(new Pin
				{
					Position = new Position(c.latitude_destination, c.longitude_destination),
					Address = c.name,
					Label = c.name,
					Type = PinType.Place,
					Icon = BitmapDescriptorFactory.FromView(new Image
					{
						Source = "ic_pin_red.png",
						WidthRequest = 25,
						HeightRequest = 30,
					}),
				});
				convoyMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(c.latitude_destination, c.longitude_destination), Distance.FromKilometers(10)));

				var titleConvoy = new Label
				{
					Text = c.name,
					FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
					FontFamily = FontRes.LabelFont,
					FontAttributes = FontAttributes.Bold,
					TextColor = ColorRes.Background,
					BackgroundColor = ColorRes.NavBar,
					HeightRequest = 50,
					HorizontalTextAlignment = TextAlignment.Center,
					VerticalTextAlignment = TextAlignment.Center,
				};

				var relativeMap = new RelativeLayout
				{
					Margin = new Thickness(0, 50, 0, 0),
					WidthRequest = App.ScreenWidth,
					HeightRequest = 200,
					IsClippedToBounds = true,
					HorizontalOptions = LayoutOptions.FillAndExpand,
					VerticalOptions = LayoutOptions.FillAndExpand,
				};

				relativeMap.Children.Add(
					new ContentViewCustom
					{
						CornerRadius = 15,
						IsClippedToBounds = true,
						HeightRequest = relativeMap.HeightRequest,
						WidthRequest = App.ScreenWidth - 20,
							//Opacity = 0.6,
							Content = convoyMap,
					},
					Constraint.Constant(0),
					Constraint.Constant(0),
					Constraint.RelativeToParent((arg) => arg.Width),
					Constraint.Constant(relativeMap.HeightRequest)
				);

				relativeMap.Children.Add(
					new ContentViewCustom
					{
						CornerRadius = 15,
						IsClippedToBounds = true,
						HeightRequest = relativeMap.HeightRequest,
						WidthRequest = App.ScreenWidth - 20,
						BackgroundColor = Color.Transparent,
						Content = new StackLayout
						{
							Children =
							{
								titleConvoy
							}
						},
					},
					Constraint.Constant(0),
					Constraint.Constant(0)
				);

				ConvoyViews.Add(relativeMap);
			}
		}

		void createLayouts()
		{
			Stack = new StackLayout
			{
				Padding = new Thickness(10),
				Spacing = 20,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
			};

			Scroll = new ScrollView();
		}

		void setLayouts()
		{
			foreach (var cv in ConvoyViews)
			{
				Stack.Children.Add(cv);
			}

			Scroll.Content = Stack;
		}
	}
}
