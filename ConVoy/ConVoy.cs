﻿using System;

using Xamarin.Forms;

namespace ConVoy
{
	public class App : Application
	{
		static public int ScreenWidth;
		static public int ScreenHeight;
		public static RestWebService RestWS;
		public static UserModel CurrentUser;

		public App()
		{
			RestWS = new RestWebService();

			CurrentUser = RestWS.GetMe();

			if (CurrentUser != null)
			{
				MainPage = new NavigationPage(new MainPage())
				{
					BarBackgroundColor = ColorRes.NavBar,
					BarTextColor = ColorRes.Background,
				};
			}
			else
			{
				MainPage = new NavigationPage(new LoginPage())
				{
					BarBackgroundColor = ColorRes.NavBar,
					BarTextColor = ColorRes.Background,
				};
			}
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
