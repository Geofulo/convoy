﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace ConVoy
{
	public class MapCustom : Map
	{
		public List<Position> RouteCoordinates { get; set; }
		public List<PinCustom> CustomPins { get; set; }

		public MapCustom()
		{
			RouteCoordinates = new List<Position>();
		}

		public static readonly BindableProperty RouteCoordinatesProperty =
			BindableProperty.Create<MapCustom, List<Position>>(
				p => p.RouteCoordinates, null);

	}
}
