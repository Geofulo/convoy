﻿using System;
using Xamarin.Forms;

namespace ConVoy
{
	public class ContentViewCustom : ContentView
	{
		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create<ContentViewCustom, Color>(
				p => p.BorderColor, Color.Black);

		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}

		public static readonly BindableProperty BorderSizeProperty =
			BindableProperty.Create<ContentViewCustom, float>(
				p => p.BorderSize, 0);

		public float BorderSize
		{
			get { return (float)GetValue(BorderSizeProperty); }
			set { SetValue(BorderSizeProperty, value); }
		}

		public static readonly BindableProperty CornerRadiusProperty =
			BindableProperty.Create<ContentViewCustom, float>(
				p => p.CornerRadius, 0);

		public float CornerRadius
		{
			get { return (float)GetValue(CornerRadiusProperty); }
			set { SetValue(CornerRadiusProperty, value); }
		}
	}
}
