﻿using System;
namespace ConVoy
{
	public static class FontRes
	{
		static string Armata = "Armata-Regular";
		static string Cambay = "Cambay-Regular";
		static string Cambay_B = "Cambay-Bold";

		//public static string EntryFont = Cambay;
		//public static string LabelFont = Cambay;
		//public static string ButtonFont = Cambay;

		public static string EntryFont = Armata;
		public static string LabelFont = Armata;
		public static string ButtonFont = Armata;
	}
}
