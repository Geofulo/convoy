﻿using System;
using Xamarin.Forms;

namespace ConVoy
{
	public static class ColorRes
	{
		//public static Color Background = Color.FromHex("FFFAFF");
		//public static Color Text = Color.FromHex("303036");
		//public static Color Button = Color.FromHex("00B9AE");
		//public static Color Button2 = Color.FromHex("037171");
		//public static Color NavBar = Color.FromHex("00B9AE");


		public static Color Background = Color.FromHex("F0F0F0");
		public static Color Text = Color.FromHex("222222");
		public static Color Button = Color.FromHex("C0392B");
		public static Color Button2 = Color.FromHex("868F9A");
		public static Color NavBar = Color.FromHex("3E6591");

	}
}
