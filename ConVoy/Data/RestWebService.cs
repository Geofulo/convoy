﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using ConVoy.Helpers;
using Newtonsoft.Json;

namespace ConVoy
{
	public class RestWebService
	{
		HttpClient Client;

		static string _uriBase = "http://convoy.geofulo13.webfactional.com/api/";
		static string _typeContent = "application/json";

		public RestWebService()
		{
			Client = new HttpClient();

			if (!string.IsNullOrEmpty(Settings.Token))
				Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", Settings.Token);
		}

		public bool Signup(UserModel usuario)
		{
			System.Diagnostics.Debug.WriteLine("Signup");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(usuario);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					Client.DefaultRequestHeaders.Authorization = null;

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var response = await Client.PostAsync(_uriBase + "auth/register/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool Login(string username, string password)
		{
			System.Diagnostics.Debug.WriteLine("Login");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var loginObject = new
					{
						username = username,
						password = password
					};

					var json = JsonConvert.SerializeObject(loginObject);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					Client.DefaultRequestHeaders.Authorization = null;

					var response = await Client.PostAsync(_uriBase + "auth/login/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<IDictionary<string, string>>(content);

						Settings.Token = jsonRes["auth_token"];

						Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", Settings.Token);

						App.CurrentUser = GetMe();

						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public UserModel GetMe()
		{
			System.Diagnostics.Debug.WriteLine("GetMe");

			var tcs = new TaskCompletionSource<UserModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBase + "auth/me/");

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<UserModel>(content);

						var user = GetUser(jsonRes.id);

						tcs.SetResult(user);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public UserModel GetUser(string id)
		{
			System.Diagnostics.Debug.WriteLine("GetMe");

			var tcs = new TaskCompletionSource<UserModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBase + "usuarios/" + id + "/");

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<UserModel>(content);

						tcs.SetResult(jsonRes);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool UpdateUsuario(UserSearchModel usuario)
		{
			System.Diagnostics.Debug.WriteLine("UpdateUsuario");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(usuario);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					var response = await Client.PutAsync(_uriBase + "usuarios/" + usuario.id + "/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool Logout()
		{
			System.Diagnostics.Debug.WriteLine("Logout");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.PostAsync(_uriBase + "auth/logout/", null);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}


		public ConvoyModel CreateConvoy(ConvoyModel convoy)
		{
			System.Diagnostics.Debug.WriteLine("CreateConvoy");

			var tcs = new TaskCompletionSource<ConvoyModel>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(convoy);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					var response = await Client.PostAsync(_uriBase + "convoy/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<ConvoyModel>(content);

						tcs.SetResult(jsonRes);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public ConvoyModel GetCurrentConvoy()
		{
			System.Diagnostics.Debug.WriteLine("GetCurrentConvoy");

			var tcs = new TaskCompletionSource<ConvoyModel>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBase + "convoy/me/?user_id=" + App.CurrentUser.id);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{	
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<ConvoyModel>(content);

						tcs.SetResult(jsonRes);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool UpdateConvoy(ConvoyModel convoy)
		{
			System.Diagnostics.Debug.WriteLine("UpdateConvoy");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(convoy);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					var response = await Client.PutAsync(_uriBase + "convoy/" + convoy.id + "/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool DeleteConvoy(string id)
		{
			System.Diagnostics.Debug.WriteLine("DeleteConvoy");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.DeleteAsync(_uriBase + "convoy/" + id + "/");

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public List<ConvoyerModel> GetConvoyers(string idConvoy)
		{
			System.Diagnostics.Debug.WriteLine("GetConvoyers");

			var tcs = new TaskCompletionSource<List<ConvoyerModel>>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBase + "convoy/convoyers/?convoy_id=" + idConvoy);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<List<ConvoyerModel>>(content);

						tcs.SetResult(jsonRes);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool CreateConvoyer(ConvoyerPostModel convoyer)
		{
			System.Diagnostics.Debug.WriteLine("CreateConvoyer");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(convoyer);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					var response = await Client.PostAsync(_uriBase + "convoyers/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool UpdateConvoyer(ConvoyerPostModel convoyer)
		{
			System.Diagnostics.Debug.WriteLine("UpdateConvoyer");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(convoyer);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					var response = await Client.PutAsync(_uriBase + "convoyers/" + convoyer.id + "/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool DeleteConvoyer(string id)
		{
			System.Diagnostics.Debug.WriteLine("DeleteConvoyer");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.DeleteAsync(_uriBase + "convoyers/" + id + "/");

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}


		public List<FriendModel> GetFriends()
		{
			System.Diagnostics.Debug.WriteLine("GetFriends");

			var tcs = new TaskCompletionSource<List<FriendModel>>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBase + "friends/" + "?user_id=" + App.CurrentUser.id);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<List<FriendModel>>(content);

						tcs.SetResult(jsonRes);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public List<UserSearchModel> SearchFriends(string username)
		{
			System.Diagnostics.Debug.WriteLine("SearchFriends");

			var tcs = new TaskCompletionSource<List<UserSearchModel>>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBase + "friends/search/?username=" + username + "&user_id=" + App.CurrentUser.id);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<List<UserSearchModel>>(content);

						tcs.SetResult(jsonRes);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool CreateFriend(FriendPostModel friend)
		{
			System.Diagnostics.Debug.WriteLine("CreateFriend");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(friend);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					var response = await Client.PostAsync(_uriBase + "friend/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool UpdateFriend(FriendPostModel friend)
		{
			System.Diagnostics.Debug.WriteLine("UpdateFriend");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var json = JsonConvert.SerializeObject(friend);

					System.Diagnostics.Debug.WriteLine("Json Object: " + json);

					var jsonContent = new StringContent(json, Encoding.UTF8, _typeContent);

					var response = await Client.PutAsync(_uriBase + "friends/" + friend.id + "/", jsonContent);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public bool DeleteFriend(string id)
		{
			System.Diagnostics.Debug.WriteLine("DeleteFriend");

			var tcs = new TaskCompletionSource<bool>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.DeleteAsync(_uriBase + "friends/" + id + "/");

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);
						tcs.SetResult(true);
					}
					else {
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(false);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(false);
				}
			}).Start();

			return tcs.Task.Result;
		}

		public List<ConvoyModel> GetHistorial()
		{
			System.Diagnostics.Debug.WriteLine("GetHistorial");

			var tcs = new TaskCompletionSource<List<ConvoyModel>>();

			new Task(async () =>
			{
				try
				{
					var response = await Client.GetAsync(_uriBase + "historial/me/?user_id=" + App.CurrentUser.id);

					System.Diagnostics.Debug.WriteLine("Request: " + response.RequestMessage);

					var content = await response.Content.ReadAsStringAsync();

					if (response.IsSuccessStatusCode)
					{
						System.Diagnostics.Debug.WriteLine("Response Content: " + content);

						var jsonRes = JsonConvert.DeserializeObject<List<ConvoyModel>>(content);

						tcs.SetResult(jsonRes);
					}
					else
					{
						System.Diagnostics.Debug.WriteLine("Error: " + response.StatusCode + " " + content);
						tcs.SetResult(null);
					}
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.WriteLine("Exception: " + e.Message);
					tcs.SetResult(null);
				}
			}).Start();

			return tcs.Task.Result;
		}
	}
}
