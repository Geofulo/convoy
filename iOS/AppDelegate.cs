﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace ConVoy.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();
			//Xamarin.FormsMaps.Init();
			Xamarin.FormsGoogleMaps.Init("AIzaSyCryh0NrLDrK8mfPuDytIbCegjdFb5VbuQ");

			App.ScreenWidth = (int)UIScreen.MainScreen.Bounds.Width;
			App.ScreenHeight = (int)UIScreen.MainScreen.Bounds.Height;

			UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
			{
				Font = UIFont.FromName(FontRes.LabelFont, 20),
			});

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}
	}
}
