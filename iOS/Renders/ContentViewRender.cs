﻿using System;
using ConVoy;
using ConVoy.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ContentViewCustom), typeof(ContentViewRender))]

namespace ConVoy.iOS
{
	public class ContentViewRender : VisualElementRenderer<ContentView>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ContentView> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				return;
			}

			var obj = (ContentViewCustom)this.Element;

			Layer.CornerRadius = obj.CornerRadius;
		}
	}
}
