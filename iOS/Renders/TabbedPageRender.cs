﻿using System;
using ConVoy;
using ConVoy.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TabbedPage), typeof(TabbedPageRender))]

namespace ConVoy.iOS
{
	public class TabbedPageRender : TabbedRenderer
	{
		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			UITextAttributes normalTextAttributes = new UITextAttributes();

			normalTextAttributes.Font = UIFont.FromName("Armata-Regular", 9.0f);
			//normalTextAttributes.Font = UIFont.FromName(FontRes.LabelFont, 12.0F);

			//this.TabBar.BarStyle = UIBarStyle.BlackTranslucent;

			this.TabBarItem.SetTitleTextAttributes(normalTextAttributes, UIControlState.Normal);
			this.TabBarItem.SetTitleTextAttributes(normalTextAttributes, UIControlState.Application);
			this.TabBarItem.SetTitleTextAttributes(normalTextAttributes, UIControlState.Highlighted);
			this.TabBarItem.SetTitleTextAttributes(normalTextAttributes, UIControlState.Selected);

			//UITabBarItem.Appearance.SetTitleTextAttributes(normalTextAttributes, UIControlState.Normal);
		}
	}
}
