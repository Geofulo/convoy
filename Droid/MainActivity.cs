﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace ConVoy.Droid
{
	[Activity(Label = "ConVoy.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			var metrics = Resources.DisplayMetrics;
			App.ScreenWidth = ConvertPixelsToDp(metrics.WidthPixels);
			App.ScreenHeight = ConvertPixelsToDp(metrics.HeightPixels);

			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);
			//Xamarin.FormsMaps.Init(this, bundle);
			Xamarin.FormsGoogleMaps.Init(this, bundle);

			LoadApplication(new App());
		}

		private int ConvertPixelsToDp(float pixelValue)
		{
			var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
			return dp;
		}
	}


}
