﻿using System;
using Android.Graphics.Drawables;
using ConVoy;
using ConVoy.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ContentViewCustom), typeof(ContentViewRender))]

namespace ConVoy.Droid
{
	public class ContentViewRender : VisualElementRenderer<ContentView>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ContentView> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				return;
			}

			var obj = (ContentViewCustom)this.Element;

			this.SetBackgroundResource(Resource.Drawable.contentRounderCorner);
			this.ClipToOutline = true;
		}

	}
}
